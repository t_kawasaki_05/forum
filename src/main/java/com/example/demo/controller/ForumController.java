package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
    @Autowired
    ReportService reportService;
    @Autowired
    CommentService commentService;

    // 投稿表示画面
    @GetMapping
    public ModelAndView top() {

        ModelAndView mav = new ModelAndView();
        List<Report> contentData = reportService.findAllReport();
        List<Comment> commentData = commentService.findAllComment();

        mav.setViewName("/top");
        mav.addObject("contents", contentData);
        mav.addObject("comments", commentData);

        return mav;
    }

    // 新規投稿画面
    @GetMapping("/new")
    public ModelAndView newContent() {

        ModelAndView mav = new ModelAndView();
        Report report = new Report();

        mav.setViewName("/new");
        mav.addObject("formModel", report);

        return mav;
    }

    // 投稿処理
    @PostMapping("/add")
    public ModelAndView addContent(@ModelAttribute("formModel") Report report) {

        reportService.saveReport(report);

        return new ModelAndView("redirect:/");
    }

    //削除処理
    @DeleteMapping("/delete/{id}")
    public ModelAndView deleteContent(@PathVariable Integer id) {

        reportService.deleteReport(id);

        return new ModelAndView("redirect:/");
    }

    // 編集画面
    @GetMapping("/edit/{id}")
    public ModelAndView editContent(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView();
        Report report = reportService.editReport(id);

        mav.addObject("formModel", report);
        mav.setViewName("/edit");

        return mav;
    }

    //編集処理
    @PutMapping("/update/{id}")
    public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

    	report.setId(id);
        Date updatedDate = new Date();
        report.setUpdatedDate(updatedDate);
        reportService.saveReport(report);

        return new ModelAndView("redirect:/");
    }

    //コメント画面
    @GetMapping("/comment/{id}")
    public ModelAndView newComment(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView();
        Report report = reportService.editReport(id);

        mav.addObject("formModel", report);
        mav.setViewName("/comment");

        return mav;
    }

    // コメント処理
    @PostMapping("/commentadd/{id}")
    public ModelAndView addComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {

        Report report = reportService.editReport(id);
        Date updatedDate = new Date();
        report.setUpdatedDate(updatedDate);
        reportService.saveReport(report);

        comment.setReportId(id);
        commentService.saveComment(comment);

        return new ModelAndView("redirect:/");
    }

    // コメント編集画面
    @GetMapping("/commentedit/{id}")
    public ModelAndView editComment(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView();
        Comment comment = commentService.editComment(id);

        mav.addObject("formModel", comment);
        mav.setViewName("/commentedit");

        return mav;
    }

    //コメント編集処理
    @PutMapping("/commentupdate/{id}")
    public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {

    	comment.setId(id);

        Date updatedDate = new Date();
        comment.setUpdatedDate(updatedDate);
        commentService.saveComment(comment);

        return new ModelAndView("redirect:/");
    }

    //削除処理
    @DeleteMapping("/commentdelete/{id}")
    public ModelAndView deleteComment(@PathVariable Integer id) {

        commentService.deleteComment(id);

        return new ModelAndView("redirect:/");
    }

    //日付絞り込み
    @PostMapping("/date")
    public ModelAndView seach(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate)
            throws ParseException {

        ModelAndView mav = new ModelAndView();
        List<Report> contentData = reportService.findByDateReport(startDate, endDate);
        List<Comment> commentData = commentService.findAllComment();

        mav.setViewName("/top");
        mav.addObject("startDate", startDate);
        mav.addObject("endDate", endDate);
        mav.addObject("contents", contentData);
        mav.addObject("comments", commentData);

        return mav;
    }
}
