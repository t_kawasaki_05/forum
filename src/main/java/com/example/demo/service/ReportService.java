package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
    @Autowired
    ReportRepository reportRepository;
    // レコード全件取得
    public List<Report> findAllReport() {
    return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
    }

    //日付取得
    public List<Report> findByDateReport(String startDate, String endDate) throws ParseException{

        if (StringUtils.isEmpty(startDate)) {
            startDate = "2022-01-01 00:00:00";
        } else {
            startDate += " 00:00:00";
        }

        if (StringUtils.isEmpty(endDate)) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formatDate = format.format(date);
            endDate = formatDate;
        } else {
            endDate += " 23:59:59";
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDay = format.parse(startDate);
        Date endDay = format.parse(endDate);
        return reportRepository.findByDate(startDay, endDay);
    }

    // レコード追加
    public void saveReport(Report report) {
    reportRepository.save(report);
    }

    //レコード削除
    public void deleteReport(Integer id) {
        reportRepository.deleteById(id);
    }

    //編集
    public Report editReport(Integer id) {
        Report report = (Report) reportRepository.findById(id).orElse(null);
        return report;
    }
}
